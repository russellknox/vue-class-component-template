const axios = require('axios')
const nodeExternals = require('webpack-node-externals')
module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'CHANGE ME',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'random-string' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: false,
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    babel: {
      plugins: [
        "transform-vue-jsx",
        "transform-runtime",
        "transform-decorators-legacy",
        "transform-class-properties"
      ],
      presets: ['es2015','stage-0', "env"]
    },
    extend (config, { isDev, isClient, isServer }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      if (isServer) {
        config.externals = [
          nodeExternals({
            // default value for `whitelist` is
            // [/es6-promise|\.(?!(?:js|json)$).{1,5}$/i]
            whitelist: [/es6-promise|\.(?!(?:js|json)$).{1,5}$/i, /^vue-awesome/]
          })
        ]
      }
    }
  },
  css: [
    '~/assets/main.less'
  ],
// ## Use to generate dynamic routes for static sites
//   generate: {
//     routes: function () {
//         return {}
//      }
//  }
}
